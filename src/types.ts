export interface IEventWithDataSet {
    target: {
        dataset: Record<string, string>
    }
}

export interface IAnyObject {
    [key: string]: any;
}
