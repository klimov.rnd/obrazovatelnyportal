import { IStore } from 'app/store/types';
import React, { Component } from 'react';
import { Header, Footer, Table } from 'app/components';
import { connect } from 'react-redux';
import { Container, LineWithGerb, GerbImg, GerbText } from './ui';
import './global-styles.css';

class App extends Component {
   render() {
      return (
         <Container>
            <Header />
            <LineWithGerb>
               <GerbImg />
               <GerbText>Добро пожаловать на корпоративный портал Министерства науки и высшего образования РФ</GerbText>
            </LineWithGerb>
            <Table />
            <Footer />
         </Container>
      );
   }
}

export default App;
