export enum ActionTypes {
    CHOOSE_TABLE = 'CHOOSE_TABLE',
}

export interface IAction {
    type: ActionTypes;
    payload: any;
}
