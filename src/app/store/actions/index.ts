import { ActionTypes } from 'app/store/actions/types';
import { AvailableTables } from 'app/store/reducers/types';

export const chooseTable = (title: AvailableTables) => ({ type: ActionTypes.CHOOSE_TABLE, payload: title });
