import { createStore } from 'redux';
import { IAction } from 'app/store/actions/types';
import { rootMiddleware } from 'app/store/middlewares';
import { rootReducer } from 'app/store/reducers';
import { IStore } from './types';

export const store = createStore<IStore, IAction, unknown, unknown>(rootReducer, rootMiddleware);
