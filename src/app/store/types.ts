import { AvailableTables } from 'app/store/reducers/types';

export interface ITableData {
    columns: string[];
    data: Array<Record<string, string>>;
}

export interface IStore {
    isMobile: boolean;
    chosenTable: AvailableTables;
}
