import { flexAlignCenter } from 'app/styled';
import styled, { css } from 'styled-components';
import { Image } from 'static/images';


// line between header and table

export const GerbText = styled.h4`
  max-width: 500px;
  
  .mobile & {
    max-width: max-content;
    margin-top: 20px;
    font-size: 15px;
  }
`;

export const GerbImg = styled(Image.Gerb)`
  display: block;
  width: 80px;
  margin-right: 20px;
`;

export const LineWithGerb = styled.div`
  ${flexAlignCenter};
  margin-bottom: 24px;
  font-size: 20px;
  font-weight: bold;
  
  .mobile & {
      flex-wrap: wrap;
  }
`;



// main container

export const Container = styled.div`
  margin: auto;
  max-width: var(--desktop-large-width);
  padding: 0 50px 20px;
  background-color: var(--main-white);
  
  .mobile & {
      box-sizing: border-box;
      margin: 0;
      padding: 0 24px 32px;
      width: 100%;
  }
`;
