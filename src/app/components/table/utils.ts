import { IAnyObject } from 'types';

export const getRowFromDataWithColumns = (row: Record<string, string>, columns: string[]) => {
   // для кейсов "only"
   if (Object.keys(row).length === 1) {
      return [row.only];
   }

   return columns.map(title => row[title]);
};

const withCommaDeleted = (value: string) => value.replace(/,/g, '');
const isNumber = (value: string) => !Number.isNaN(Number(value));

export const getSortedByField = (rows: IAnyObject[], field: string, reverse?: boolean) => {
   const unlinkedRows = [...rows];
   const sortedRows = unlinkedRows
      .sort(({ [field]: value1 }, { [field]: value2 }) => {
         if (value1 === value2) {
            return 0;
         }

         const correctedValue1 = withCommaDeleted(value1);
         const correctedValue2 = withCommaDeleted(value2);

         if (isNumber(correctedValue1) && isNumber(correctedValue2)) {
            return Number(correctedValue1) - Number(correctedValue2);
         }

         return value1 < value2 ? -1 : 1;
      });

   return reverse ? sortedRows.reverse() : sortedRows;
};
