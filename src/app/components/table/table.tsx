import { afterChartImage } from 'app/components/table/constants';
import { getRowFromDataWithColumns, getSortedByField } from 'app/components/table/utils';
import { AvailableTables, Store } from 'app/store/reducers/types';
import { IStore, ITableData } from 'app/store/types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import tableData from 'static/data/data.json';
import { Image, ImgNames } from 'static/images';
import { v4 as uuidv4 } from 'uuid';
import { Wrap, TableBody, TableHead, TableWrap, TableCell, TableRow, ChartWrap, TableTitle } from './ui';


enum SortingPrefixes {
    AtoZ= 'a-z',
    ZtoA= 'z-a',
}

interface IProps {
    chosenTable: AvailableTables;
    columns: ITableData['columns'];
    data: ITableData['data'];
}

interface IState {
    sortBy: string;
    sortType: SortingPrefixes;
}

class _Table extends Component<IProps, IState> {
    state = {
       sortBy: '',
       sortType: SortingPrefixes.AtoZ,
    }

    setsortBy = (newSortBy: string) => {
       const { sortBy, sortType } = this.state;


       const isSameSortType = newSortBy === sortBy;
       const newSortType = isSameSortType
          ? sortType === SortingPrefixes.AtoZ ? SortingPrefixes.ZtoA : SortingPrefixes.AtoZ
          : SortingPrefixes.AtoZ;

       this.setState({ sortBy: newSortBy, sortType: newSortType });
    }

    renderHeaderCells = () => {
       const { columns } = this.props;
       const { sortBy, sortType } = this.state;

       return columns.map(title => (
          <TableCell
             inHeader
             isActive={title === sortBy}
             onClick={() => this.setsortBy(title)}
             fromAtoZ={sortType === SortingPrefixes.AtoZ}
             key={title}
          >
             {title}
          </TableCell>
       ));
    }

    renderCells = (row: ITableData['columns']) => {
       return row.map(title => (
          <TableCell key={uuidv4()}>
             {title}
          </TableCell>
       ));
    }

    get sortedRows() {
       const { sortBy, sortType } = this.state;
       const { data, columns } = this.props;

       const sortedRows = getSortedByField(data, sortBy, sortType === SortingPrefixes.ZtoA);

       return sortedRows.map(row => getRowFromDataWithColumns(row, columns));
    }

    render() {
       const { chosenTable } = this.props;
       const chartImage = afterChartImage[chosenTable];
       // @ts-ignore
       const ChartImageComponent = Image[chartImage];


       return (
          <>
             <TableTitle>{chosenTable}</TableTitle>
             <Wrap>
                <TableWrap>
                   <TableRow header>
                      {this.renderHeaderCells()}
                   </TableRow>
                   {this.sortedRows.map(row => (
                      <TableRow key={uuidv4()}>
                         {this.renderCells(row)}
                      </TableRow>
                   ))}
                </TableWrap>
             </Wrap>
             {chartImage && <ChartWrap><ChartImageComponent /></ChartWrap>}
          </>
       ); 
    }
}

const mapStateToProps = (store: IStore) => {
   const chosenTable = store[Store.chosenTable] as AvailableTables;
   // @ts-ignore
   const { columns, data } = tableData[chosenTable] as ITableData;

   return ({ columns, data, chosenTable: store.chosenTable });
};

export const Table = connect(mapStateToProps)(_Table);
