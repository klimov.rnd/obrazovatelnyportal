import { AvailableTables } from 'app/store/reducers/types';
import React from 'react';
import { Image, ImgNames } from 'static/images';

export const afterChartImage: Partial<Record<AvailableTables, ImgNames>> = {
   [AvailableTables['Распределение численности студентов по курсам']]: ImgNames.standardChart,
   [AvailableTables['Распределение приема, численности студентов и выпуска специалистов образовательных организаций']]: ImgNames.radialChart,
};
