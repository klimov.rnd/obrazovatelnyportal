import React, { Component } from 'react';
import { AboutText,FooterWrap, SkypeLogo, TelegramLogo, UniversityLogo, WhatsappLogo } from './ui';

export class Footer extends Component {
   render() {
      return (
         <FooterWrap>
            <AboutText>Проектный офис МФТИ</AboutText>
            <TelegramLogo />
            <WhatsappLogo />
            <SkypeLogo />
            <UniversityLogo />
         </FooterWrap>
      );
   }
}
