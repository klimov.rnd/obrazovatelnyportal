import { flexAlignCenter } from 'app/styled';
import { Image } from 'static/images';
import styled, { css } from 'styled-components';

export const AboutText = styled.p`
  font-size: 14px;
  margin-right: 16px;
  font-weight: bold;
  
  .mobile & {
      font-size: 12.8px;
      font-weight: 300;
  }
`;

const InfoLogoStyles = css`
  width: 45px;
  margin-right: 8px;
`;

export const TelegramLogo = styled(Image.TelegramIcon)`${InfoLogoStyles}`;
export const WhatsappLogo = styled(Image.WhatsappIcon)`${InfoLogoStyles}`;
export const SkypeLogo = styled(Image.SkypeIcon)`${InfoLogoStyles}`;
export const UniversityLogo = styled(Image.UniversityIcon)`
   ${InfoLogoStyles};
   width: 80px;
`;

export const FooterWrap = styled.footer`
  ${flexAlignCenter};
  justify-content: flex-end;
`;
