import { AvailableTables } from 'app/store/reducers/types';

export interface ISubMenuItem {
    title: AvailableTables;
}

export interface IMenuItem {
    title: string;
    link?: string;
    disabled?: boolean;
    children?: ISubMenuItem[];
}

export const MENU_STRUCTURE: IMenuItem[] = [
   {
      title: 'Достижение результатов по национальным проектов ',
      disabled: true,
   },
   {
      title: 'Статистика по Университетам',
      children: [
         { title: AvailableTables['Распределение приема по направлениям'] },
         { title: AvailableTables['Распределение приема по годам'] },
         { title: AvailableTables['Распределение численности студентов по курсам'] },
         { title: AvailableTables['Распределение приема, численности студентов и выпуска специалистов образовательных организаций'] },
         { title: AvailableTables['Распределение выпуска'] },
         { title: AvailableTables['Распределение численности студентов, приема и выпуска по гражданству'] },
         // { title: AvailableTables['zzzzzzzzzzz'] },
      ]
   },
   {
      title: 'Иностранные студенты',
      disabled: true,
   },
   {
      title: 'Бюджетные показатели',
      disabled: true,
   },
   {
      title: 'Аналитические отчеты',
      disabled: true,
   },
   {
      title: 'Данные из МИРЭА',
      disabled: true,
   },
   {
      title: 'Карта',
      link: 'https://www.figma.com/proto/3wBFYcamfwHOiHRcrczLfM/%D0%9A%D0%B0%D1%80%D1%82%D0%B0?node-id=21%3A4&viewport=506%2C373%2C0.16934116184711456&scaling=min-zoom'
   },
];
