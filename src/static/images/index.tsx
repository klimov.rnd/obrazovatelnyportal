import React, { Component } from 'react';
import { IAnyObject } from 'types';
// @ts-ignore
import Logo from './logo.png';
// @ts-ignore
import Background from './background.png';
// @ts-ignore
import Gerb from './gerb.png';
// @ts-ignore
import TelegramIcon from './telegram-icon.png';
// @ts-ignore
import SkypeIcon from './skype-icon.png';
// @ts-ignore
import WhatsappIcon from './whatsapp-icon.png';
// @ts-ignore
import RadialChart from './radial_chart.png';
// @ts-ignore
import StandardChart from './standard_chart.png';
// @ts-ignore
import UniversityIcon from './university-logo.png';


export const ImgSources: Record<ImgNames, string> = {
   Logo,
   Gerb,
   TelegramIcon,
   SkypeIcon,
   WhatsappIcon,
   Background,
   UniversityIcon,
   RadialChart,
   StandardChart
};

export enum ImgNames {
    logo = 'Logo',
    gerb = 'Gerb',
    telegramIcon = 'TelegramIcon',
    skypeIcon = 'SkypeIcon',
    whatsappIcon = 'WhatsappIcon',
    background = 'Background',
    universityIcon = 'UniversityIcon',
    radialChart = 'RadialChart',
    standardChart = 'StandardChart',
}

interface IProps {
    name: ImgNames,
    alt?: string,
}

export class Image extends Component<IProps>{
    static defaultProps = {
       name: 'logo',
       alt: '',
    }

    static Logo = (props: IAnyObject) => <Image {...props} name={ImgNames.logo} />;
    static Gerb = (props: IAnyObject) => <Image {...props} name={ImgNames.gerb} />;
    static RadialChart = (props: IAnyObject) => <Image {...props} name={ImgNames.radialChart} />;
    static StandardChart = (props: IAnyObject) => <Image {...props} name={ImgNames.standardChart} />;
    static TelegramIcon = (props: IAnyObject) => <Image {...props} name={ImgNames.telegramIcon} />;
    static SkypeIcon = (props: IAnyObject) => <Image {...props} name={ImgNames.skypeIcon} />;
    static WhatsappIcon = (props: IAnyObject) => <Image {...props} name={ImgNames.whatsappIcon} />;
    static UniversityIcon = (props: IAnyObject) => <Image {...props} name={ImgNames.universityIcon} />;

    render() {
       const { name, alt, ...rest } = this.props;
       const src = ImgSources[name];

       return <img {...rest} alt={alt} src={src} />;
    }
}
